<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class User extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        error_reporting(E_ALL);
        $this->load->model('Booking_model');
        $this->load->model('keys');
        $this->load->helper('directory');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    /**
     * To login
     */
    public function login_post() {
        if ($this->post('username') && $this->post('password') && $this->post('user_type')) {
//        if ($this->post('username') && $this->post('password')) {
            $con['conditions'] = array(
                'username' => $this->post('username'),
                'password' => $this->post('password'),
                'user_type' => $this->post('user_type'),
                'status' => 1
            );
            $checkLogin = $this->Booking_model->login($con);
            if ($checkLogin) {
                $generatedKey = $this->_generate_key();
                $data = array(
                    'user_id' => $checkLogin['id'],
                    'key' => $generatedKey,
                    'level' => '1'
                );
                $return = $this->keys->insert($data);
                $condn = array();
                $condn['user_id'] = $checkLogin['id'];
                $loginData = array();
                $loginData['logged_in'] = date('Y-m-d H:i:s');
                $this->Booking_model->logUpdate($loginData, $condn);
                $message = [
                    'status' => TRUE,
                    'message' => 'Logged in successfully',
                    'user_id' => $checkLogin['id'],
                    'key' => $generatedKey
                ];
            } else {
                $message = 'Invalid Username and Password';
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => $message,
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
            }
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Email or Password not supplied'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code            
        }

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    /**
     * To logout
     */
    public function logout_post() {
        if ($this->post('key')) {
            $message = [
                'status' => TRUE,
                'message' => 'Logged out successfully'
            ];
            $conditions = array(
                'key' => $this->post('key')
            );
            $this->keys->delete($conditions);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Wrong Try'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code            
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    /**
     * To handle change password
     */
    public function change_password_() {
        if ($this->post('email') && $this->post('newpassword') && $this->post('key')) {
            $conditions = array(
                'email' => $this->post('email'),
                'status' => 'ACTIVE'
            );
            $checkUser = $this->directors->checkUser($conditions);
            if ($checkUser > 0) {
                $userId = $checkUser;
                $keycodn['conditions'] = array(
                    'user_id' => $userId,
                    'key' => $this->post('key')
                );
                $available = $this->keys->getRows($keycodn);
                if (!$available) {
                    // Set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Wrong Try'
                            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code                      
                } else {
                    $this->directors->changePassword($userId, $this->post('newpassword'));

                    $message = [
                        'status' => TRUE,
                        'message' => 'Password has been changed successfully'
                    ];
                }
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Email or User does not exists'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code                 
            }
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Wrong Try'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code            
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }
    
    public function createuser_post(){
        
        if ($this->post('firstname') && $this->post('username') && $this->post('password') && $this->post('user_type')) {
            
             $con = array(
                'firstname' => $this->post('firstname'),
                'lastname' => $this->post('lastname'),
                'username' => $this->post('username'),
                'password' => $this->post('password'),
                'user_type' => $this->post('user_type'),
                'created_at' => date("y-m-d H:i:s"),
                'status' => $this->post('status'),
            ); 
            $this->Booking_model->create_user($con);
            
        $message = [
                      'status' => TRUE,
                      'message' => 'User Created Successfully',
                     // 'dc_details' => $data
                  ];
        } else {
            $message = 'process is unsuccessfull';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        
    }
    public function edituser_post() {
        if ($this->post('id')) {
            $userid = $this->post('id');
            $condition = array('userid' => $userid);
            $data['user'] = $this->Booking_model->get_user($condition);
            //$data['user'][0]['status'] = ($data['user'][0]['deleted_at'] == NULL) ? 'ACTIVE' : 'INACTIVE';
            //echo '<pre>'; print_r($data); exit;
            $message = [
                'status' => TRUE,
                'message' => 'Data displayed successfully',
                'data' => $data['user'],
            ];
        } else {
            $message = 'Invalid Parameter supplied';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP
    }

    public function updateusers_post() {
        if ($this->post('id')) {
            $data = $this->input->post();
            $userid = $this->post('id');
            $condition = array('userid' => $userid);
            $data['user'] = $this->Booking_model->get_user($condition);

            $data['user'][0]['username'] = $this->post('username');
            $data['user'][0]['password'] = $this->post('password');
            $data['user'][0]['user_type'] = $this->post('user_type');
            $data['user'][0]['firstname'] = $this->post('firstname');
            $data['user'][0]['lastname'] = $this->post('lastname');
            $data['user'][0]['lastname'] = $this->post('lastname');
            $data['user'][0]['status'] = $this->post('lastname');
            // echo '<pre>'; print_r($data); exit;
            if ($userid == ($data['user'][0]['id'])) {
                if ($this->post('username') == ($data['user'][0]['username'])) {
                    $this->Booking_model->update_user($condition, $data);
                    $message = [
                        'status' => TRUE,
                        'message' => 'User updated successfully',
                    ];
                } else {
//            $this->username_exists();
                    $username = $this->input->post('username');
                    $exists = $this->Booking_model->username_exists($username);
                    $count = count($exists);
                    //echo $count;
                    if (empty($count)) {
                        $this->Booking_model->update_user($condition, $data);
                        $message = [
                            'status' => TRUE,
                            'message' => 'User updated successfully',
                        ];
                    } else {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'Username is Available',
                                ], REST_Controller::HTTP_NOT_FOUND);
                    }
                }
            }
        } else {
            $message = 'Invalid Parameter supplied';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) be
    }

    public function viewusers_post() {
        $search = '';
        if ($this->post('search')) {
            $search = $this->post('search');
        }
        $data["users"] = $this->Booking_model->get_users_list($search);
        //$data["superadmins"] = $this->Booking_model->get_super_user_list();
        $message = [
            'status' => TRUE,
            'message' => 'user list displayed Successfully',
            'userlist' => $data
        ];
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }
	
	public function hallslist_post() {
        $search = '';
        if ($this->post('search')) {
            $search = $this->post('search');
        }
        $data["halls"] = $this->Booking_model->get_halls_list($search);
        $message = [
            'status' => TRUE,
            'message' => 'halls list displayed Successfully',
            'userlist' => $data
        ];
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }
    
    public function createbooking_post(){
        
        if ($this->post('name') && $this->post('phone') && $this->post('booked_for')) {
            
             $con = array(
                'name' => $this->post('name'),
                'phone' => $this->post('phone'),
                'email' => $this->post('email'),
                'address' => $this->post('address'),
                'booked_for' => $this->post('booked_for'),
                'booked_on' => $this->post('booked_on'),
                'from' => $this->post('from'),
                'to' => $this->post('to'),
                'slot' => $this->post('slot'),
                'total_price' => $this->post('total_price'),
                'paid' => $this->post('paid'),
                'note' => $this->post('note'),
                'booked_on' => date("y-m-d H:i:s"),
                'status' => $this->post('status'),
            ); 
             //echo '<pre>'; print_r($con); exit;
            $this->Booking_model->create_booking($con);
            
        $message = [
                      'status' => TRUE,
                      'message' => 'Booking Created Successfully',
                     // 'dc_details' => $data
                  ];
        } else {
            $message = 'process is unsuccessfull';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        
    }    
    public function editbooking_post() {
        if ($this->post('id')) {
            $userid = $this->post('id');
            $condition = array('booking' => $userid);
            $data['booking'] = $this->Booking_model->get_booking($condition);
            //$data['user'][0]['status'] = ($data['user'][0]['deleted_at'] == NULL) ? 'ACTIVE' : 'INACTIVE';
            //echo '<pre>'; print_r($data); exit;
            $message = [
                'status' => TRUE,
                'message' => 'Data displayed successfully',
                'data' => $data['booking'],
            ];
        } else {
            $message = 'Invalid Parameter supplied';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP
    } 
    
    public function updatebooking_post() {
        if ($this->post('id')) {
            $data = $this->input->post();
            $bookingid = $this->post('id');
            $condition = array('booking' => $bookingid);
            $data['booking'] = $this->Booking_model->get_booking($condition);

            $data['booking'][0]['name'] = $this->post('name');
            $data['booking'][0]['phone'] = $this->post('phone');
            $data['booking'][0]['email'] = $this->post('email');
            $data['booking'][0]['address'] = $this->post('address');
            $data['booking'][0]['booked_for'] = $this->post('booked_for');
            $data['booking'][0]['booked_on'] = $this->post('booked_on');
            $data['booking'][0]['from'] = $this->post('from');
            $data['booking'][0]['to'] = $this->post('to');
            $data['booking'][0]['slot'] = $this->post('slot');
            $data['booking'][0]['total_price'] = $this->post('total_price');
            $data['booking'][0]['paid'] = $this->post('paid');
            $data['booking'][0]['note'] = $this->post('note');
            $data['booking'][0]['status'] = $this->post('status');

            if ($bookingid == ($data['booking'][0]['id'])) {
                    $this->Booking_model->update_booking($condition, $data);
                    $message = [
                        'status' => TRUE,
                        'message' => 'Booking updated successfully',
                        //'data' => $data,
                    ];
            }
        } else {
            $message = 'Invalid Parameter supplied';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) be
    }    
    
    
    public function disablebooking_post() {
        if ($this->post('id')) {
            $bookingid = $this->post('id');
            $condition = array('booking' => $bookingid);
            $data['booking'] = $this->Booking_model->get_booking($condition);
            $data['booking'][0]['status'] = $this->post('status');
            
            if ($bookingid == ($data['booking'][0]['id'])) {
                    $this->Booking_model->update_booking($condition, $data);
                    $message = [
                        'status' => TRUE,
                        'message' => 'Booking updated successfully',
                        //'data' => $data,
                    ];
            }    
            

            $message = [
                'status' => TRUE,
                'message' => 'Data displayed successfully',
                'data' => $data['booking'],
            ];
        } else {
            $message = 'Invalid Parameter supplied';
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => $message,
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code 
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP
    } 
    
    private function _generate_key() {
        do {
            // Generate a random salt
            $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);

            // If an error occurred, then fall back to the previous method
            if ($salt === FALSE) {
                $salt = hash('sha256', time() . mt_rand());
            }

            $new_key = substr($salt, 0, config_item('rest_key_length'));
        } while ($this->_key_exists($new_key));

        return $new_key;
    }

    /* Private Data Methods */

    private function _get_key($key) {
        return $this->rest->db
                        ->where(config_item('rest_key_column'), $key)
                        ->get(config_item('rest_keys_table'))
                        ->row();
    }

    private function _key_exists($key) {
        return $this->rest->db
                        ->where(config_item('rest_key_column'), $key)
                        ->count_all_results(config_item('rest_keys_table')) > 0;
    }

    private function _insert_key($key, $data) {
        $data[config_item('rest_key_column')] = $key;
        $data['date_created'] = function_exists('now') ? now() : time();

        return $this->rest->db
                        ->set($data)
                        ->insert(config_item('rest_keys_table'));
    }

    private function _update_key($key, $data) {
        return $this->rest->db
                        ->where(config_item('rest_key_column'), $key)
                        ->update(config_item('rest_keys_table'), $data);
    }

    private function _delete_key($key) {
        return $this->rest->db
                        ->where(config_item('rest_key_column'), $key)
                        ->delete(config_item('rest_keys_table'));
    }

}
